module gitlab.com/fisherprime/go-miner

go 1.18

require (
	github.com/heetch/confita v0.10.0
	github.com/sirupsen/logrus v1.8.1
	github.com/sjwhitworth/golearn v0.0.0-20211014193759-a8b69c276cd8
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/gonum/blas v0.0.0-20181208220705-f22b278b28ac // indirect
	github.com/gonum/lapack v0.0.0-20181123203213-e4cdc5a0bff9 // indirect
	github.com/gonum/matrix v0.0.0-20181209220409-c518dec07be9 // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/guptarohit/asciigraph v0.5.5 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/rocketlaunchr/dataframe-go v0.0.0-20211025052708-a1030444159b // indirect
	golang.org/x/exp v0.0.0-20220613132600-b0d781184e0d // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f // indirect
	golang.org/x/sys v0.0.0-20220615213510-4f61da869c0c // indirect
	gonum.org/v1/gonum v0.11.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
