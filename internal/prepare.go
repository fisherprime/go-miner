package gominer

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"reflect"
	"strings"

	"github.com/sjwhitworth/golearn/base"
	"github.com/sjwhitworth/golearn/filters"
)

const (
	PrepDedup      = "dedup"
	PrepDiscretize = "discretize"
	PrepRmPct      = "remove-pct"
	PrepSelAttr    = "select-attributes"
	PrepSplit      = "split"
)

var (
	ErrInvalidSplitPercentage = errors.New("invalid data set split percentage")
	ErrUndefinedInstances     = errors.New("the current `DataSet` lacks instances")

	ErrRecoveredPanic = errors.New("recovered from panic")
)

// getAttributeSpecs obtaines a list of `AttributeSpec`s from the `base.FixedDataGrid`.
//
// REF: `base.copyFixedDataGridStructure` && `base.ResolveAllAttributes`.
// FIXME: This operation lacks the sort by attribute index operation; may lead to bugs.
//
// The golearn library prefers to panic instead of propagating errors.
func getAttributeSpecs(ctx context.Context, data base.FixedDataGrid) (specs []base.AttributeSpec, err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		attrs := data.AllAttributes()
		specs = make([]base.AttributeSpec, len(attrs))

		var spec base.AttributeSpec
		for index := range attrs {
			if spec, err = data.GetAttribute(attrs[index]); err != nil {
				return
			}
			specs[index] = spec
		}
	}

	return
}

// locateRow in a `base.DenseInstances`.
//
// REF: `base.MapOverRows`.
//
// Fasters approach is to use `(*base.DenseInstances).RowString()`.
func locateRow(ctx context.Context, data *base.DenseInstances, row [][]byte) (loc int, err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		loc = -1

		var specs []base.AttributeSpec
		if specs, err = getAttributeSpecs(ctx, data); err != nil {
			return
		}

		err = data.MapOverRows(specs, func(values [][]byte, rowIndex int) (resl bool, err error) {
			if reflect.DeepEqual(values, row) {
				// `resl` is set to false to terminate the `MapOverRows` operation; the row has been
				// located.
				resl = false
				loc = rowIndex
			}
			return
		})
	}

	return
}

// GetWorkingInstances for use in the mining process.
func (d *DataSet) GetWorkingInstances(_ context.Context) (src base.FixedDataGrid, err error) {
	// Use the prepared instances if it exists; else use the raw instances.
	switch {
	case d.TrainInstances != nil:
		src = d.TrainInstances
	case d.RawInstances != nil:
		src = d.RawInstances
	default:
		// Should never occur.
		err = ErrUndefinedInstances
	}

	return
}

// Discretize the `DataSet`.
func (d *DataSet) Discretize(ctx context.Context, index int) (err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		var significance float64

		options := d.Operations[index].Options[1:]

		flags := flag.NewFlagSet("Discretization config", flag.ContinueOnError)
		flags.Float64Var(&significance, "s", 0.999, "The discretization significance")
		if err = flags.Parse(options); err != nil {
			return
		}

		var inst base.FixedDataGrid
		if inst, err = d.GetWorkingInstances(ctx); err != nil {
			return
		}

		// Use Chi-Merge for discretization.
		fl := filters.NewChiMergeFilter(inst, significance)
		if err = fl.Train(); err != nil {
			return
		}

		d.TrainInstances = base.NewLazilyFilteredInstances(inst, fl)
	}

	return
}

// Dedup the `DataSet`.
func (d *DataSet) Dedup(ctx context.Context, _ int) (err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		defer func() {
			// The golearn library loves to panic.
			if r := recover(); r != nil {
				err = fmt.Errorf("%w: %v", ErrRecoveredPanic, r)
			}
		}()

		var inst base.FixedDataGrid
		if inst, err = d.GetWorkingInstances(ctx); err != nil {
			return
		}

		// Own implementation.
		/* var specs []base.AttributeSpec
		 * if specs, err = getAttributeSpecs(ctx, src); err != nil {
		 *     return
		 * } */

		specs := base.ResolveAllAttributes(inst)
		d.TrainInstances = base.NewStructuralCopy(inst)
		err = inst.(*base.DenseInstances).MapOverRows(specs, func(values [][]byte, _ int) (resl bool, err error) {
			// `resl` should always be true else the `MapOverRows` operation is terminated.
			resl = true

			var loc int
			if loc, err = locateRow(ctx, d.TrainInstances.(*base.DenseInstances), values); loc > -1 || err != nil {
				return
			}

			// Appending to the new data set.
			//
			// The index passed to this function will reference the
			// index in the original `base.FixedDataGrid` that may have duplicates..
			_, rowIndex := d.TrainInstances.Size()
			for index := range values {
				d.TrainInstances.(*base.DenseInstances).Set(specs[index], rowIndex, values[index])
			}

			return
		})
	}

	return
}

func (d *DataSet) SelectAttributes(ctx context.Context, index int) (err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		defer func() {
			// The golearn library loves to panic.
			if r := recover(); r != nil {
				err = fmt.Errorf("%w: %v", ErrRecoveredPanic, r)
			}
		}()

		var attribVal string

		options := d.Operations[index].Options[1:]

		flags := flag.NewFlagSet("Attribute selection config", flag.ContinueOnError)
		flags.StringVar(&attribVal, "a", "", "A comma-separated list of attributes to retain")
		if err = flags.Parse(options); err != nil {
			return
		}

		argAttributes := strings.Split(attribVal, ",")

		var inst base.FixedDataGrid
		if inst, err = d.GetWorkingInstances(ctx); err != nil {
			return
		}

		denseSrc := inst.(*base.DenseInstances)

		allAttribs := denseSrc.AllAttributes()
		selectedAttribs, fAIndex := make([]base.Attribute, len(argAttributes)), 0
		for index := range allAttribs {
			a := allAttribs[index]
			aName := a.GetName()
			for _, sAttrib := range argAttributes {
				if aName == sAttrib {
					selectedAttribs[fAIndex] = a
					fAIndex++
				}
			}
		}
		specs := base.ResolveAttributes(denseSrc, selectedAttribs)

		d.TrainInstances = base.NewStructuralCopy(inst)
		err = denseSrc.MapOverRows(specs, func(values [][]byte, _ int) (resl bool, err error) {
			// `resl` should always be true else the `MapOverRows` operation is terminated.
			resl = true

			// Appending to the new data set.
			_, rowIndex := d.TrainInstances.Size()
			for index := range values {
				d.TrainInstances.(*base.DenseInstances).Set(specs[index], rowIndex, values[index])
			}

			return
		})
	}

	return
}

// RemovePCT from the `DataSet`.
//
// This operation shuffles the `DataSet`.
func (d *DataSet) RemovePCT(ctx context.Context, index int) (err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		var percentage float64

		options := d.Operations[index].Options[1:]

		flags := flag.NewFlagSet("Instances removal config", flag.ContinueOnError)
		flags.Float64Var(&percentage, "p", 10, "The percentage of instances to be removed")
		if err = flags.Parse(options); err != nil {
			return
		}
		if percentage < 1 {
			err = ErrInvalidSplitPercentage
			return
		}

		var inst base.FixedDataGrid
		if inst, err = d.GetWorkingInstances(ctx); err != nil {
			return
		}
		_, d.TrainInstances = base.InstancesTrainTestSplit(base.Shuffle(inst), percentage/100)
	}

	return
}

// Split the `DataSet` into train/test instances.
//
// This operation shuffles the `DataSet`.
func (d *DataSet) Split(ctx context.Context, index int) (err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		var percentage float64

		options := d.Operations[index].Options[1:]

		flags := flag.NewFlagSet("Dataset split config", flag.ContinueOnError)
		flags.Float64Var(&percentage, "p", 70, "The percentage of train instances to split")
		if err = flags.Parse(options); err != nil {
			return
		}
		if percentage < 1 {
			err = ErrInvalidSplitPercentage
			return
		}

		var inst base.FixedDataGrid
		if inst, err = d.GetWorkingInstances(ctx); err != nil {
			return
		}
		d.TrainInstances, d.TestInstances = base.InstancesTrainTestSplit(base.Shuffle(inst), percentage/100)
	}

	return
}
