package gominer

import (
	"context"
	"errors"
	"fmt"
	"path/filepath"
	"strings"

	"github.com/sjwhitworth/golearn/base"
)

type (
	// DataSet contains the source dataset, it's operations & results.
	DataSet struct {
		// SrcPath defines the location of the `DataSet` on the local filesystem.
		SrcPath,
		// SrcFile defines the base name for the `DataSet` without the file extension.
		SrcFile,
		// FileSaveSuffix defines the suffix used to identity output generated for the `DataSet`
		// given it's immediate context.
		FileSaveSuffix string

		// RawInstances defines the instances loaded from the source file
		RawInstances *base.DenseInstances

		// TrainInstances defines the instances used to train a model.
		TrainInstances,
		// TestInstances defines the instances used to test a model.
		TestInstances base.FixedDataGrid

		// Operations is an ordered listing of data preparation & classification/clustering
		// operations. The operations will be executed in the order they are defined.
		Operations []Operation

		// InternalLog contains logs for operations perfomed on the `DataSet`.
		InternalLog *strings.Builder
	}

	// Operation defines some operation on a dataset.
	//
	// Allows for ordered operations with varying options.
	Operation struct {
		// Identifier defines a unique identifier for the data preparation, clustering or classification
		// task.
		Identifier string `config:"identifier"`

		// SaveModel defines whether a model should be saved for the current operation. This is used
		// for the classification/clustering tasks only.
		SaveModel bool `config:"save_model"`

		// Options defines the parameters passed onto the `Identifier` identified task.
		Options []string `config:"options"`
	}
)

const (
	// Valid `DataSet` extensions.
	ExtArff = ".arff"
	ExtCSV  = ".csv"

	loadedFileFmt = "Loaded file: %s"
)

var (
	ErrUnsupportedFileType = errors.New("unsupported file type")
)

// newDataset initializes a `DataSet`.
func newDataset(ctx context.Context, path string, hasHeader bool, ops ...Operation) (dSet *DataSet, err error) {
	dSet = &DataSet{
		SrcPath:        "",
		SrcFile:        "",
		FileSaveSuffix: "",
		RawInstances:   &base.DenseInstances{},
		TrainInstances: nil,
		TestInstances:  nil,
		Operations:     []Operation{},
		InternalLog:    &strings.Builder{},
	}

	suffix := filepath.Ext(path)
	switch suffix {
	case ExtArff:
		dSet.RawInstances, err = base.ParseDenseARFFToInstances(path)
	case ExtCSV:
		dSet.RawInstances, err = base.ParseCSVToInstances(path, hasHeader)
	default:
		err = fmt.Errorf("%w: %s", ErrUnsupportedFileType, suffix)
	}
	if err != nil {
		return
	}

	if err = dSet.AddOperations(ctx, ops...); err != nil {
		return
	}

	dSet.SrcPath = path
	dSet.SrcFile = strings.Split(filepath.Base(path), ".")[0]

	dSet.LogOperation(ctx, fmt.Sprintf(loadedFileFmt, path))

	return
}

// AddOperations to a `DataSet`.
func (d *DataSet) AddOperations(_ context.Context, ops ...Operation) (err error) {
	lenAdditionalOps := len(ops)
	if lenAdditionalOps < 1 {
		return
	}

	var errs []error
	for index := range ops {
		op := &ops[index]
		switch op.Identifier {
		case PrepDedup, PrepDiscretize, PrepRmPct, PrepSelAttr, PrepSplit:
		case OpTrainTest, OpCrossValidate:
		default:
			errs = append(errs, fmt.Errorf("%w: %s", ErrUnknownOperation, op.Identifier))
		}
	}
	if len(errs) > 0 {
		err = fmt.Errorf("%+v", errs)
		return
	}

	d.Operations = append(d.Operations, ops...)

	return
}
