package gominer

import (
	"context"
	"errors"
	"fmt"
	"sync"
)

// Resl defines a struct to propagate an error on execution failure or the `DataSource`'s index on
// success for some data mining processes.
type Resl struct {
	err   error
	index int
}

var (
	ErrUndefinedSources = errors.New("undefined data sources")
	ErrUnconfigured     = errors.New("the library is not configured, call `Configure(cfg *Config)`")
)

// Run executes the hostsgen main operation.
func Run() (err error) {
	if Cfg == nil {
		err = ErrUnconfigured
		return
	}

	var (
		ctx    context.Context
		cancel context.CancelFunc
	)
	if Cfg.ExecutionTimeout > 0 {
		ctx, cancel = context.WithTimeout(context.Background(), Cfg.ExecutionTimeout)
	} else {
		ctx, cancel = context.WithCancel(context.Background())
	}
	defer cancel()

	numSources := len(Cfg.Sources)
	if numSources < 1 {
		err = ErrUndefinedSources
		return
	}

	var wg sync.WaitGroup
	wg.Add(numSources)

	reslChan := make(chan Resl, numSources)

	for index := range Cfg.Sources {
		index := index
		go func(rChan chan Resl) {
			var e error
			defer func() {
				if e != nil {
					rChan <- Resl{err: e}
				}
				wg.Done()
			}()

			src := Cfg.Sources[index]

			var dSet *DataSet
			if dSet, e = newDataset(ctx, src.Path, src.HasHeader, src.Operations...); e != nil {
				return
			}
			if e = dSet.Mine(ctx); e != nil {
				return
			}
		}(reslChan)
	}

	var (
		errs        []error
		usedSources []int
	)
	go func() {
		for {
			r, ok := <-reslChan
			if !ok {
				break
			}

			if r.index > -1 {
				usedSources = append(usedSources, r.index)
				continue
			}
			errs = append(errs, r.err)
		}
	}()
	wg.Wait()

	// TODO: Utilize the used sources.

	if len(errs) > 0 {
		err = fmt.Errorf("%+v", errs)
	}

	return
}
