package gominer

import (
	"context"
	"flag"
	"fmt"

	"github.com/sjwhitworth/golearn/perceptron"
)

// FIXME: `perceptron.AveragePerceptron.Fit` does not return an error.
func configureAvgPerceptron(ctx context.Context, options []string) (cl *perceptron.AveragePerceptron, saveSuffix string, err error) {
	var (
		args struct {
			features int

			learningRate,
			startThre,
			trainErr float64
		}
	)

	flags := flag.NewFlagSet("Average Perceptron config", flag.ContinueOnError)
	flags.IntVar(&args.features, "f", 1, "The distance function")
	flags.Float64Var(&args.learningRate, "l", 1, "The searching algorithm")
	flags.Float64Var(&args.startThre, "s", 1, "The neighbours per cluster")
	flags.Float64Var(&args.trainErr, "t", 1, "The neighbours per cluster")
	if err = flags.Parse(options); err != nil {
		return
	}

	/* if args.startThre > 1 {
	 *     return
	 * }
	 * if args.trainErr > 1 {
	 *     return
	 * } */

	cl = perceptron.NewAveragePerceptron(args.features, args.learningRate, args.startThre, args.trainErr)
	saveSuffix = fmt.Sprint(MinerAvgPerceptron, genSaveSuffix(ctx, args))

	return
}
