package gominer

import (
	"context"
	"time"
)

type (
	// Config defines the configuration file for the binary.
	Config struct {
		Runtime `config:"runtime" yaml:"runtime"`

		// LogFile defines the location to log the program's execution. If not configured, the log is
		// output onto stdout.
		LogFile string `config:"log_file" yaml:"log_file"`

		// Sources defines multiple `DataSource`s to perform some data mining task on.
		Sources []DataSource `config:"sources" yaml:"sources"`
	}

	// Runtime defines configuration options that affect the runtime environment.
	Runtime struct {
		// ExecutionTimeout defines the maximum run time for the binary as a `time.Duration`. If not
		// configured, the binary will fun infinitely.
		ExecutionTimeout time.Duration `config:"execution_timeout" yaml:"execution_timeout"`
	}

	// DataSource defines a data set and the operations to be perfomed on it.
	DataSource struct {
		// Path defines the location of a data source on the local filesystem.
		Path string `config:"path" yaml:"path"`

		// HasHeader indicates whether a data set contains field headers.
		HasHeader bool `config:"has_header" yaml:"has_header"`

		// Operations is an ordered listing of data preparation & classification/clustering
		// operations. The operations will be executed in the order they are defined.
		Operations []Operation `config:"operations" yaml:"operations"`
	}
)

var (
	// Cfg is a global variable containing the configured runtime environment.
	Cfg *Config
)

// NewDefaultConfig generates the default `Config`.
func NewDefaultConfig() Config {
	return Config{
		Runtime: Runtime{
			ExecutionTimeout: 1 * time.Hour,
		},
		LogFile: "",
		Sources: []DataSource{
			{
				Path:      "tests/sample.csv",
				HasHeader: true,
				Operations: []Operation{
					{
						Identifier: PrepDedup,
						SaveModel:  false,
						Options:    []string{},
					},
					{
						Identifier: PrepDiscretize,
						SaveModel:  false,
						Options:    []string{},
					},
				},
			},
			{
				Path:      "tests/sample.arff",
				HasHeader: true,
				Operations: []Operation{
					{
						Identifier: PrepRmPct,
						SaveModel:  false,
						Options:    []string{},
					},
				},
			},
		},
	}
}

// Configure sets up the runtime environment before execution.
func Configure(_ context.Context, cfg *Config) {
	Cfg = cfg
}
