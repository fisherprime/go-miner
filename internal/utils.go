package gominer

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"time"
)

// genSaveSuffix generates a model saving suffix using the classifier/clusterer's options.
func genSaveSuffix(_ context.Context, args interface{}) (suffix string) {
	s := reflect.TypeOf(args)
	numFields := s.NumField()
	if numFields < 1 {
		return
	}

	// TODO: Refine this operation; current state will produce messy filenames.
	buffer := strings.Builder{}
	for index := 0; index < numFields; index++ {
		field := s.Field(index)
		_, _ = fmt.Fprintf(&buffer, "_%s(%s)", field.Name, reflect.ValueOf(field))
	}
	_, _ = fmt.Fprintf(&buffer, "_%s", time.Now().Format(time.RFC3339))

	return buffer.String()
}

// OverwriteFile overwrites the contents of a file with the supplied data.
func OverwriteFile(path string, data []byte) (err error) {
	var (
		outputPath string
		file       *os.File
	)

	if outputPath, err = filepath.Abs(path); err != nil {
		return
	}

	// nolint: gosec // "G304" is sorted out by `filepath.Abs`.
	if file, err = os.OpenFile(outputPath, os.O_CREATE|os.O_RDWR, 0600); err != nil {
		err = fmt.Errorf("failed to open file %s: %w", outputPath, err)
		return
	}

	_, err = file.Write(data)
	_ = file.Close()

	return
}

// ReadFile reads the contents of a file.
func ReadFile(path string) (data []byte, err error) {
	var (
		file *os.File
	)

	if path, err = filepath.Abs(path); err != nil {
		return
	}

	// nolint: gosec // "G304" is sorted out by `filepath.Abs`.
	if file, err = os.OpenFile(path, os.O_RDONLY, 0600); err != nil {
		err = fmt.Errorf("failed to open file %s: %w", path, err)
		return
	}

	_, err = file.Read(data)
	_ = file.Close()

	return
}
