package gominer

import (
	"context"
	"flag"
	"fmt"

	"github.com/sjwhitworth/golearn/knn"
)

func configureKNNR(ctx context.Context, options []string) (cl *knn.KNNRegressor, saveSuffix string, err error) {
	var (
		args struct {
			distFunc string
		}
	)

	flags := flag.NewFlagSet("KNN regressor config", flag.ContinueOnError)
	flags.StringVar(&args.distFunc, "d", DistEuclidean, "The distance function")
	if err = flags.Parse(options); err != nil {
		return
	}

	switch args.distFunc {
	case DistEuclidean, DistManhattan, DistCosine:
		// Ok
	default:
		err = fmt.Errorf("%w: %s", ErrUnknownDistFuncID, args.distFunc)
		return
	}

	cl = knn.NewKnnRegressor(args.distFunc)
	saveSuffix = fmt.Sprint(RegKnn, genSaveSuffix(ctx, args))

	return
}
