package gominer

import (
	"context"
	"errors"
	"fmt"
	"path/filepath"

	"github.com/sjwhitworth/golearn/base"
)

const (
	// OP_DEDUP uint32 = 1 << (32 - 1 - iota)
	OpCrossValidate = "cross-validate"
	OpTrainTest     = "train-test"
)

const (
	MinerPca = "principal-component-analysis"

	MinerBernoulli = "bernoulli-nb"

	MinerKnn = "knn-classifier"

	MinerDtree      = "decision-tree-classifier"
	MinerID3        = "id3"
	MinerIsolationF = "isolation-forest"
	MinerRandForest = "random-forest"
	MinerRandTree   = "random-tree"

	MinerLinearReg   = "liner-regression"
	MinerLinearSvc   = "linear-svc"
	MinerLogisticReg = "logistic-regression"

	MinerAvgPerceptron = "average-perceptron"
	MinerMultiLayerNet = "multi-layer-net"
	MinerMultiLinSvc   = "multi-linear-svc"

	RegDtree = "decision-tree-regressor"
	RegKnn   = "knn-regressor"
)

const (
	modelExt = ".model"
)

var (
	ErrUnknownOperation       = errors.New("unknown operation")
	ErrUndefinedMiningOptions = errors.New("undefined options for classification/clustering task")
	ErrUnknownMiner           = errors.New("unknown miner/classifier")
)

// Mine performs classification, clustering, … operations.
func (d *DataSet) Mine(ctx context.Context) (err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		for index := range d.Operations {
			operation := d.Operations[index]

			switch operation.Identifier {
			case PrepDiscretize:
				err = d.Discretize(ctx, index)
			case PrepDedup:
				err = d.Dedup(ctx, index)
			case OpCrossValidate:
				err = d.CrossValidate(ctx, index)
			case OpTrainTest:
				err = d.Train(ctx, index)
			default:
				err = fmt.Errorf("%w: %s", ErrUnknownOperation, operation.Identifier)
			}

			if err != nil {
				return
			}
		}
	}

	return
}

// Train a model using the provided `DataSet`.
func (d *DataSet) Train(ctx context.Context, index int) (err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		options := d.Operations[index].Options[1:]

		lenOptions := len(options)
		if lenOptions < 1 {
			err = ErrUndefinedMiningOptions
			return
		}

		var cl base.Classifier
		if cl, err = d.ConfigureMiner(ctx, index); err != nil {
			return
		}

		var inst base.FixedDataGrid
		if inst, err = d.GetWorkingInstances(ctx); err != nil {
			return
		}
		if err = cl.Fit(inst); err != nil {
			return
		}

		if d.Operations[index].SaveModel {
			modelSavePath := fmt.Sprintf("%s_%s%s", d.SrcFile, d.FileSaveSuffix, modelExt)
			if modelSavePath, err = filepath.Abs(modelSavePath); err != nil {
				return
			}

			if err = cl.Save(modelSavePath); err != nil {
				return
			}
		}

		if d.TestInstances == nil {
			return
		}

		var predictions base.FixedDataGrid
		if predictions, err = cl.Predict(d.TestInstances); err != nil {
			return
		}
		if err = d.LogDefaultEvalResults(ctx, predictions); err != nil {
			return
		}
	}

	return
}

func (d *DataSet) CrossValidate(ctx context.Context, _ int) (err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:

		// TODO: Add cross-validation logic.
	}

	return
}

// ConfigureMiner configures an arbitrary data mining algorithm given an identifier & optional options.
func (d *DataSet) ConfigureMiner(ctx context.Context, index int) (cl base.Classifier, err error) {
	options := d.Operations[index].Options

	var miner string
	miner, options = options[0], options[1:]

	switch miner {
	case MinerID3:
		cl, d.FileSaveSuffix, err = configureID3(ctx, options)
	default:
		err = fmt.Errorf("%w: %s", ErrUnknownMiner, miner)
	}

	return
}
