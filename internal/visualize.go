package gominer

import (
	"context"
	"strings"

	"github.com/sjwhitworth/golearn/base"
)

func (d *DataSet) Visualize(ctx context.Context, cl base.Classifier) (err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		buffer := strings.Builder{}
		buffer.WriteString(cl.String())

		// TODO: Add visualization logic.
	}

	return
}
