package gominer

import (
	"context"
	"fmt"
	"strings"

	"github.com/sjwhitworth/golearn/base"
	"github.com/sjwhitworth/golearn/evaluation"
)

const (
	evalResultDelim = "# ---"
)

// LogDefaultEvalResults onto the `DataSet`'s internal log.
func (d *DataSet) LogDefaultEvalResults(ctx context.Context, predictions base.FixedDataGrid) (err error) {
	var cMatrix map[string]map[string]int
	if cMatrix, err = evaluation.GetConfusionMatrix(d.TestInstances, predictions); err != nil {
		return
	}

	d.LogOperation(ctx, evaluation.GetSummary(cMatrix))

	return
}

// REF:
// https://github.com/sjwhitworth/golearn/blob/00d4cfd91a1b/examples/trees/isolationForest/isolation_forest.go
func (d *DataSet) LogEvalAnomalies(ctx context.Context, preds []float64) (err error) {
	buffer := strings.Builder{}

	sum := 0.0
	min := 1.0
	size := len(preds)

	// Let's find the average and minimum Anomaly Score for normal data.
	for index := 0; index < size; index++ {
		temp := preds[index]
		sum += temp
		if temp < min {
			min = temp
		}
	}
	avg := sum / float64(size)
	fmt.Fprintf(&buffer, "avg score: %f\nmin: %f", avg, min)

	// Now let's print the anomaly scores for the outliers.
	//
	// Poor outlier visualization, taking anything > average.
	fmt.Fprintf(&buffer, "Anomaly Scores for outliers are: ")
	for index := size; index < size; index++ {
		if pred := preds[index]; pred > avg {
			fmt.Fprint(&buffer, "      ", pred)
		}
	}

	d.LogOperation(ctx, buffer.String())

	return
}

// LogOperation onto the `DataSet`'s internal log.
//
// TODO: Look into adding stdout not logged into log file.
func (d *DataSet) LogOperation(ctx context.Context, resl string) {
	_, _ = fmt.Fprintf(d.InternalLog, "\n%s\n%s", evalResultDelim, resl)
}
