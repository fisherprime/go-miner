package gominer

import (
	"context"
	"errors"
	"flag"
	"fmt"

	"github.com/sjwhitworth/golearn/base"
	"github.com/sjwhitworth/golearn/ensemble"
	"github.com/sjwhitworth/golearn/trees"
)

const (
	RuleGenInfoGain = "info-gain"
	RuleGenGini     = "gini"
)

var (
	ErrUnknownRuleGenID = errors.New("unknown rule generator identifier")
)

// configureID3 configures an ID3 miner.
func configureID3(ctx context.Context, options []string) (cl base.Classifier, saveSuffix string, err error) {
	var (
		args struct {
			testPruneRatio float64
			ruleGenerator  string
		}
	)

	// TODO: Validate passing `-h` generates help.
	flags := flag.NewFlagSet("ID3 config", flag.ContinueOnError)
	flags.Float64Var(&args.testPruneRatio, "p", 0.999, "The test-prune ratio")
	flags.StringVar(&args.ruleGenerator, "r", RuleGenInfoGain, "The rule generator")
	if err = flags.Parse(options); err != nil {
		return
	}

	var rGen trees.RuleGenerator
	switch args.ruleGenerator {
	case RuleGenInfoGain:
		rGen = new(trees.InformationGainRatioRuleGenerator)
	case RuleGenGini:
		rGen = new(trees.GiniCoefficientRuleGenerator)
	default:
		err = fmt.Errorf("%w: %s", ErrUnknownRuleGenID, args.ruleGenerator)
		return
	}

	cl = trees.NewID3DecisionTreeFromRule(args.testPruneRatio, rGen)
	saveSuffix = fmt.Sprint(MinerID3, genSaveSuffix(ctx, args))

	return
}

func configureRandomTree(ctx context.Context, options []string) (cl base.Classifier, saveSuffix string, err error) {
	var (
		args struct {
			attributes int
		}
	)

	flags := flag.NewFlagSet("RandomTree config", flag.ContinueOnError)
	flags.IntVar(&args.attributes, "a", 1, "The number of attributes to randomly select")
	if err = flags.Parse(options); err != nil {
		return
	}

	cl = trees.NewRandomTree(args.attributes)
	saveSuffix = fmt.Sprint(MinerRandTree, genSaveSuffix(ctx, args))

	return
}

// configureRandomForest configures a RandomForest miner.
func configureRandomForest(ctx context.Context, options []string) (cl base.Classifier, saveSuffix string, err error) {
	var (
		args struct {
			forestSize int
			features   int
		}
	)

	flags := flag.NewFlagSet("RandomForest config", flag.ContinueOnError)
	flags.IntVar(&args.forestSize, "s", 1, "The number of trees in the forest")
	flags.IntVar(&args.features, "f", 1, "The number of features to build each tree")
	if err = flags.Parse(options); err != nil {
		return
	}

	cl = ensemble.NewRandomForest(args.forestSize, args.features)
	saveSuffix = fmt.Sprint(MinerRandForest, genSaveSuffix(ctx, args))

	return
}

// FIXME: `trees.IsolationForest.Fit` does not return an error.
func configureIsolationForest(ctx context.Context, options []string) (cl trees.IsolationForest, saveSuffix string, err error) {
	var (
		args struct {
			nTrees   int
			maxDepth int
			subSpace int
		}
	)

	flags := flag.NewFlagSet("IsolationForest config", flag.ContinueOnError)
	flags.IntVar(&args.nTrees, "n", 1, "The number of trees in the forest")
	flags.IntVar(&args.maxDepth, "m", 1, "The maximum depth of each tree")
	flags.IntVar(&args.subSpace, "s", 1, "The number of data points to use per tree")
	if err = flags.Parse(options); err != nil {
		return
	}

	cl = trees.NewIsolationForest(args.nTrees, args.maxDepth, args.subSpace)
	saveSuffix = fmt.Sprint(MinerRandForest, genSaveSuffix(ctx, args))

	return
}
