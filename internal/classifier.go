package gominer

import (
	"context"
	"errors"
	"flag"
	"fmt"

	"github.com/sjwhitworth/golearn/base"
	"github.com/sjwhitworth/golearn/knn"
)

const (
	DistEuclidean = "euclidean"
	DistManhattan = "manhattan"
	DistCosine    = "cosine"

	SearchLinear = "linear"
	SearchKDTree = "kdtree"
)

var (
	ErrUnknownDistFuncID  = errors.New("unknown distance function identifier")
	ErrUnknownSearchAlgID = errors.New("unknown search function identifier")
)

func configureKNNC(ctx context.Context, options []string) (cl base.Classifier, saveSuffix string, err error) {
	var (
		args struct {
			distFunc,
			algorithm string
			neighbours int
		}
	)

	flags := flag.NewFlagSet("K-Nearest Neighbours config", flag.ContinueOnError)
	flags.StringVar(&args.distFunc, "d", DistEuclidean, "The distance function")
	flags.StringVar(&args.algorithm, "a", SearchLinear, "The searching algorithm")
	flags.IntVar(&args.neighbours, "n", 1, "The neighbours per cluster")
	if err = flags.Parse(options); err != nil {
		return
	}

	switch args.distFunc {
	case DistEuclidean, DistManhattan, DistCosine:
		// Ok
	default:
		err = fmt.Errorf("%w: %s", ErrUnknownDistFuncID, args.distFunc)
		return
	}

	switch args.algorithm {
	case SearchLinear, SearchKDTree:
		// Ok
	default:
		err = fmt.Errorf("%w: %s", ErrUnknownSearchAlgID, args.algorithm)
		return
	}

	cl = knn.NewKnnClassifier(args.distFunc, args.algorithm, args.neighbours)
	saveSuffix = fmt.Sprint(MinerKnn, genSaveSuffix(ctx, args))

	return
}
