package gominer

import (
	"context"
	"flag"
	"fmt"

	"github.com/sjwhitworth/golearn/ensemble"
)

// configureMultiLinearSVC configures a MinerMultiLinSvc miner.
//
// `ensemble.MultiLinearSVC` lacks the `String` method.
func configureMultiLinearSVC(ctx context.Context, options []string) (cl *ensemble.MultiLinearSVC, saveSuffix string, err error) {
	var (
		args struct {
			loss,
			penalty string

			dual bool

			c,
			eps float64

			weights string
		}
	)

	flags := flag.NewFlagSet("MinerMultiLinSvc config", flag.ContinueOnError)
	flags.StringVar(&args.loss, "l", "", "")
	flags.StringVar(&args.penalty, "p", "", "")
	flags.BoolVar(&args.dual, "d", false, "")
	flags.Float64Var(&args.c, "c", 0.0, "")
	flags.Float64Var(&args.eps, "e", 0.0, "")
	flags.StringVar(&args.weights, "w", "", "")

	if err = flags.Parse(options); err != nil {
		return
	}

	// TODO: Parse args.weights to weights.
	var weights map[string]float64

	cl = ensemble.NewMultiLinearSVC(args.loss, args.penalty, args.dual, args.c, args.eps, weights)
	saveSuffix = fmt.Sprint(MinerMultiLinSvc, genSaveSuffix(ctx, args))

	return
}
