package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/file"
	"github.com/sirupsen/logrus"
	gominer "gitlab.com/fisherprime/go-miner/internal"
	"gopkg.in/yaml.v3"
)

const (
	configExample = "config-sample.yml"
	project       = "Generic data mining thing"
)

var (
	Build   string
	Version string
)

func main() {
	var (
		args struct {
			ConfigFile         string
			PrintVersion       bool
			GenerateConfigFile bool
		}

		err error
	)

	flags := flag.NewFlagSet(project, flag.ExitOnError)
	flags.StringVar(&args.ConfigFile, "c", "config.yml", "Path to configuration file")
	flags.BoolVar(&args.PrintVersion, "V", false, "Print version information")
	flags.BoolVar(&args.GenerateConfigFile, "g", false, fmt.Sprintf("Generate sample configuration file: %s", configExample))

	if err = flags.Parse(os.Args[1:]); err != nil {
		logrus.Fatal(err)
	}

	switch {
	case args.PrintVersion:
		fmt.Printf("%s v%s \nBuild: %s\n", project, Version, Build)
		return
	case args.GenerateConfigFile:
		var output []byte
		output, err = yaml.Marshal(gominer.NewDefaultConfig())
		if err != nil {
			logrus.Fatal(err)
		}

		if err = gominer.OverwriteFile(configExample, output); err != nil {
			logrus.Fatal(err)
		}

		return
	}

	loader := confita.NewLoader(file.NewBackend(args.ConfigFile))

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	cfg := gominer.NewDefaultConfig()
	if err = loader.Load(ctx, &cfg); err != nil {
		logrus.Fatal(err)
	}
	gominer.Configure(ctx, &cfg)

	if err := gominer.Run(); err != nil {
		logrus.Fatal(err)
	}
}
